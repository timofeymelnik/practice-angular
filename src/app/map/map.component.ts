import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ElementRef} from '@angular/core';
import {} from '@types/googlemaps';
type Pos = { lat: number; lng: number };

@Component({
    selector: 'g-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
    @ViewChild('mapCanvas') mapCanvas: ElementRef;

    private pos: Pos = {
        lat: 46.4598865,
        lng: 30.5717048
    };
    private map: any;

    constructor() {
    }

    ngOnInit() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((pos) => {
                this.pos.lat = pos.coords.latitude;
                this.pos.lng = pos.coords.longitude;
            });
        }
    }

    static _mapCenter(lat:number, lng:number) {
        return new google.maps.LatLng(lat, lng)
    }

    _placeMarker(location:Pos) {
        return new google.maps.Marker({
            position: location,
            map: this.map
        });
    }

    ngAfterViewInit() {
        this.map = new google.maps.Map(this.mapCanvas.nativeElement, {
            zoom: 11,
            center: MapComponent._mapCenter(this.pos.lat, this.pos.lng),
            disableDefaultUI: true
        });

        this.map.addListener('click', (event: google.maps.MouseEvent) => {
            this._placeMarker({lat: event.latLng.lat(), lng: event.latLng.lng()})
        });
    }
}
